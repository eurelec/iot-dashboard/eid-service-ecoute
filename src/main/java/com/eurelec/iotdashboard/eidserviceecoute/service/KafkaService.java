package com.eurelec.iotdashboard.eidserviceecoute.service;

import com.eurelec.iotdashboard.eidserviceecoute.Payload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaService {

    @Autowired
    private KafkaTemplate<String, Payload> kafkaTemplate;

    public void sendMessage(Payload msg) {
        kafkaTemplate.send("iotHeartbeat", "heartbeat", msg);
    }

}
