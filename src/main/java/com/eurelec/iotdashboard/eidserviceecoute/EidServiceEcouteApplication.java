package com.eurelec.iotdashboard.eidserviceecoute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class EidServiceEcouteApplication {

    public static void main(String[] args) {
        SpringApplication.run(EidServiceEcouteApplication.class, args);
    }

}
