package com.eurelec.iotdashboard.eidserviceecoute.client;


import com.eurelec.iotdashboard.eidserviceecoute.dto.DeviceStatusDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(
        name = "CacheClient",
        url = "http://localhost:8081/api/v1/status"
)
public interface CacheClient {
    @GetMapping(value = "/{id}")
    DeviceStatusDto getStatus(@PathVariable("id") String id);

}
