package com.eurelec.iotdashboard.eidserviceecoute.api.v1;

import com.eurelec.iotdashboard.eidserviceecoute.Payload;
import com.eurelec.iotdashboard.eidserviceecoute.client.CacheClient;
import com.eurelec.iotdashboard.eidserviceecoute.dto.DeviceStatusDto;
import com.eurelec.iotdashboard.eidserviceecoute.service.KafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/iot-heartbeat")
public class IoTHeartbeatController {

    @Autowired
    KafkaService kafkaService;
    @Autowired
    CacheClient cacheClient;

    @PostMapping()
    public ResponseEntity<DeviceStatusDto> sendMessage(@RequestBody Payload message) {
        kafkaService.sendMessage(message);
        return ResponseEntity.ok(cacheClient.getStatus(message.getId()));
    }

}
